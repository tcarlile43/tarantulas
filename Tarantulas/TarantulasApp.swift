//
//  TarantulasApp.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/24/20.
//

import SwiftUI

@main
struct TarantulasApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            MainView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
                .onAppear(perform: UIApplication.shared.addTapGestureRecognizer)
        }
    }
}

extension UIApplication: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
}

extension UIApplication {
    func addTapGestureRecognizer() {
        guard let window = windows.first else {
            return
        }
        
        let tapGesture = UITapGestureRecognizer(target: window, action: #selector(UIView.endEditing(_:)))
        tapGesture.requiresExclusiveTouchType = false
        tapGesture.cancelsTouchesInView = false
        tapGesture.delegate = self
        window.addGestureRecognizer(tapGesture)
    }
}

struct KeyboardSafeArea: ViewModifier {
    func body(content: Content) -> some View {
        content.ignoresSafeArea(.container)
    }
}

extension View {
    func keyboardStyle() -> some View {
        modifier(KeyboardSafeArea())
    }
}
