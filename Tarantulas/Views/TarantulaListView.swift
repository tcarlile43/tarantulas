//
//  ContentView.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/24/20.
//

import SwiftUI
import CoreData

struct TarantulaListView: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    @State private var userMessage: String = ""
    @State private var displayMessage: Bool = false

    @FetchRequest
    private var tarantulas: FetchedResults<Tarantula>
    
    init(sortKey: SortKey, ascending: Bool) {
        self._tarantulas = FetchRequest(entity: Tarantula.entity(),
                                        sortDescriptors: [sortKey.toSortDescriptor(ascending: ascending)],
                                        animation: .default)
    }

    var body: some View {
        List {
            Section(header: Text("Summary")) {
                Text("\(tarantulas.count) Tarantula\(tarantulas.count == 1 ? "" : "s")")
                Text(Gender.toShorthandNotation(tarantulas.map { $0.tarantulaGender }))
            }
            Section(header: Text("Tarantulas")) {
                ForEach(tarantulas, id: \.self) { tarantula in
                    NavigationLink(destination: TarantulaDetailView(tarantula: tarantula)) {
                        TarantulaListCellView(tarantula: tarantula)
                    }
                }
                .onDelete(perform: deleteItems)
            }
        }
        .alert(isPresented: $displayMessage) {
            Alert(title: Text("Delete Error"), message: Text(userMessage), dismissButton: .default(Text("Ok")))
        }
    }

    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { tarantulas[$0] }.forEach(viewContext.delete)

            do {
                try viewContext.saveSafely()
            } catch {
                userMessage = "\(error): \(error.localizedDescription)"
                displayMessage.toggle()
            }
        }
    }
}
