//
//  TabbedMainView.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 12/2/20.
//

import SwiftUI

struct MainView: View {
    @AppStorage("tarantulaSortKey") var tarantulaSortKey: SortKey = .Date_Acquired
    @AppStorage("tarantulaSortAscending") var tarantulaSortAscending: Bool = true
    
    @State private var showSortSheet = false
    
    var body: some View {
        NavigationView {
            MainTarantulaView()
        }
    }
}

struct MainTarantulaView: View {
    @AppStorage(Settings.TARANTULA_SORT_KEY) var tarantulaSortKey: SortKey = .Date_Acquired
    @AppStorage(Settings.TARANTULA_SORT_ASCENDING) var tarantulaSortAscending: Bool = true
    @AppStorage(Settings.PREFERRED_SIZE_UNIT) var preferredSizeUnit: SizeUnit = .DEFAULT
    
    @StateObject private var tarantulaModel: Tarantula = Tarantula.newModel()
    @State private var showOptionsSheet = false
    
    private let email = "tarantulatracker.bugreport@gmail.com"
    
    var body: some View {
        TarantulaListView(sortKey: tarantulaSortKey, ascending: tarantulaSortAscending)
            .navigationTitle("My Tarantulas")
            .navigationBarItems(leading: Button(action: {
                showOptionsSheet.toggle()
            }) {
                Text("Options")
            }, trailing: NavigationLink(destination: TarantulaView(tarantula: tarantulaModel)) {
                        Image(systemName: "plus").foregroundColor(.blue)
                    })
            .sheet(isPresented: $showOptionsSheet) {
                Form {
                    Section(header: Text("Sort")) {
                        Picker("Sort By", selection: $tarantulaSortKey) {
                            ForEach(SortKey.allCases, id: \.self) {
                                Text($0.description).tag($0)
                            }
                        }.pickerStyle(MenuPickerStyle())
                        Toggle("Ascending", isOn: $tarantulaSortAscending)
                    }
                    Section(header: Text("Preferences")) {
                        HStack {
                            Text("Size Unit: ")
                            Picker("Size Unit", selection: $preferredSizeUnit) {
                                ForEach(SizeUnit.allCases, id: \.self) {
                                    Text($0.rawValue).tag($0)
                                }
                            }.pickerStyle(SegmentedPickerStyle())
                        }
                    }
                    Section(header: Text("Report Bug")) {
                        Button(action: {
                            if let url = URL(string: "mailto:\(email)") {
                              if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url)
                              } else {
                                UIApplication.shared.openURL(url)
                              }
                            }
                        }) {
                            Text("Report")
                        }
                    }
                    Section() {
                        Button(action: {
                            showOptionsSheet.toggle()
                        }) {
                            Text("Close Options")
                        }
                    }
                }
            }
    }
}
