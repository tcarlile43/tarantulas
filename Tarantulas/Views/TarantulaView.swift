//
//  NewTarantula.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/24/20.
//

import SwiftUI
import CoreData

struct TarantulaView: View {
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.managedObjectContext) private var viewContext
    
    @ObservedObject private var tarantula: Tarantula
    @State private var image: Image? = .CAMERA
    @State private var userMessage = ""
    @State private var displayMessage = false
    
    
    init(tarantula: Tarantula) {
        self.tarantula = tarantula
    }
    
    var currentImage: Image {
        tarantula.modelImage ?? Image.CAMERA
    }
    
    var body: some View {
        Form {
            Section(header: Text("Tarantula")) {
                HStack(spacing: 10) {
                    currentImage
                        .resizable()
                        .frame(width: Image.WIDTH, height: Image.HEIGHT, alignment: .topLeading)
                        .modifier(ImageSelector(uiImage: $tarantula.modelPhoto) {
                            tarantula.photo = nil
                            image = .DEFAULT
                        })
                    VStack(alignment: .leading) {
                        TextField("Name", text: $tarantula.modelName)
                        Divider()
                        TextField("Genus", text: $tarantula.modelGenus)
                        Divider()
                        TextField("Species", text: $tarantula.modelSpecies)
                        Divider()
                        TextField("Common Name", text: $tarantula.modelCommonName)
                    }
                }
            }
            Section(header: Text("Timeline")) {
                DatePicker("DOB", selection: $tarantula.modelDob, displayedComponents: .date)
                DatePicker("Date Acquired", selection: $tarantula.modelDateAcquired, displayedComponents: .date)
                Toggle("Is Mature", isOn: $tarantula.isMature)
                if tarantula.isMature {
                    DatePicker("Date Matured", selection: $tarantula.modelDateMatured, displayedComponents: .date)
                }
                Toggle("Is Deceased", isOn: $tarantula.isDeceased)
                if tarantula.isDeceased {
                    DatePicker("Date Deceased", selection: $tarantula.modelDateDeceased, displayedComponents: .date)
                }
            }
            Section(header: Text("Details")) {
                Picker("Gender", selection: $tarantula.modelGender) {
                    ForEach(Gender.allCases, id: \.self) {
                        Text($0.rawValue).tag($0)
                    }
                }
                Picker("World", selection: $tarantula.modelWorld) {
                    ForEach(World.allCases, id: \.self) {
                        Text($0.rawValue).tag($0)
                    }
                }
                Picker("Behavior", selection: $tarantula.modelBehavior) {
                    ForEach(Behavior.allCases, id: \.self) {
                        Text($0.rawValue).tag($0)
                    }
                }
            }
            Section(header: Text("Size")) {
                Stepper(value: $tarantula.modelSize, in: 0...100, step: .SIZE_STEP) {
                    Text("Size: \(tarantula.modelSize, specifier: "%.2f")")
                }
                Picker("Unit", selection: $tarantula.modelSizeUnit) {
                    ForEach(SizeUnit.allCases, id: \.self) {
                        Text($0.rawValue).tag($0)
                    }
                }
                Stepper(value: $tarantula.modelInstar) {
                    Text("Instar: \(tarantula.modelInstar.description)")
                }
            }
            Section(header: Text("Notes")) {
                TextView(text: $tarantula.modelNotes).frame(minHeight: 200)
            }
            Section() {
                Button(action: {
                    UIApplication.endEditing()
                    
                    if !tarantula.isManaged() {
                        if !tarantula.isDeceased {
                            tarantula.dateDeceased = nil
                        }
                        
                        if !tarantula.isMature {
                            tarantula.dateMatured = nil
                        }
                        
                        let _ = tarantula.copyTo(context: viewContext)
                        tarantula.resetModel()
                    }
                    
                    do {
                        try viewContext.saveSafely()
                        presentationMode.wrappedValue.dismiss()
                    } catch {
                        userMessage = "\(error): \(error.localizedDescription)"
                        displayMessage.toggle()
                    }
                }) {
                    Text("Save Tarantula")
                }
            }
        }
        .keyboardStyle()
        .alert(isPresented: $displayMessage) {
            Alert(title: Text("Save Failure"), message: Text(userMessage), dismissButton: .default(Text("Ok")))
        }
        .onAppear {
            if !tarantula.isManaged() {
                tarantula.sizeUnit = UserDefaults.standard.string(forKey: Settings.PREFERRED_SIZE_UNIT) ?? SizeUnit.DEFAULT.rawValue
            }
        }
        .navigationTitle("Tarantula")
    }
}
