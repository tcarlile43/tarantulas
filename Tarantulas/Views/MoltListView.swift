//
//  MoltListView.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/29/20.
//

import SwiftUI

struct MoltListView: View {
    private static let DATE_FORMATTER: DateFormatter = {
            let d = DateFormatter()
            d.dateFormat = "MMM d, y"
            return d
        }()
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @StateObject private var moltModel: Molt = Molt.newModel()
    @ObservedObject private var tarantula: Tarantula
    @FetchRequest private var molts: FetchedResults<Molt>
    @State private var userMessage: String = ""
    @State private var displayMessage: Bool = false
    
    init(tarantula: Tarantula) {
        self.tarantula = tarantula
        self._molts = FetchRequest(entity: Molt.entity(),
                                   sortDescriptors: [NSSortDescriptor(keyPath: \Molt.date, ascending: false)],
                                   predicate: NSPredicate(format: "tarantula = %@", tarantula),
                                   animation: .default)
    }
    
    var body: some View {
        List {
            Section(header: Text("Summary")) {
                Text("\(molts.count) Molt\(molts.count == 1 ? "" : "s")")
            }
            Section(header: Text("Molts")) {
                NavigationLink(destination: MoltView(tarantula: tarantula, molt: moltModel)) {
                    Text("Add Molt").foregroundColor(.blue)
                }
                ForEach(molts, id: \.self) { molt in
                    NavigationLink(destination: MoltView(tarantula: tarantula, molt: molt)) {
                        Text(MoltListView.DATE_FORMATTER.string(from: molt.modelDate))
                    }
                }
                .onDelete(perform: deleteItems)
            }
        }
        .alert(isPresented: $displayMessage) {
            Alert(title: Text("Save Failure"), message: Text(userMessage), dismissButton: .default(Text("Ok")))
        }
    }
    
    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            // TODO: This is necessary due to a system bug. See: https://developer.apple.com/forums/thread/668299
            viewContext.perform {
                offsets.map { molts[$0] }.forEach(viewContext.delete)

                do {
                    try viewContext.saveSafely()
                } catch {
                    userMessage = "\(error): \(error.localizedDescription)"
                    displayMessage.toggle()
                }
            }
        }
    }
}
