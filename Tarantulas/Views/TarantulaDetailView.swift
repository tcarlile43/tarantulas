//
//  TarantulaDetailView.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/29/20.
//

import SwiftUI

private enum Tab: Hashable {
    case Tarantula
    case Feeding
    case Molt
    
    func getTitle() -> String {
        switch self {
        case .Tarantula:
            return "Details"
        case .Feeding:
            return "Feeding"
        case .Molt:
            return "Molts"
        }
    }
}

struct TarantulaDetailView: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    @ObservedObject private var tarantula: Tarantula
    @State private var selection: Tab = .Tarantula
    
    init(tarantula: Tarantula) {
        self.tarantula = tarantula
    }
    
    var body: some View {
        TabView(selection: $selection) {
            TarantulaView(tarantula: tarantula)
                .tabItem {
                    Image(systemName: "t.circle")
                }.tag(Tab.Tarantula)
            FeedingListView(tarantula: tarantula)
                .tabItem {
                    Image(systemName: "ant")
                }.tag(Tab.Feeding)
            MoltListView(tarantula: tarantula)
                .tabItem {
                    Image(systemName: "m.square")
                }.tag(Tab.Molt)
        }.navigationTitle(selection.getTitle())
    }
}
