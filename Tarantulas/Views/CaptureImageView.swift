//
//  CaptureImageView.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/27/20.
//

import Foundation
import SwiftUI
import UIKit

/// Create a wrapper to use the image UIImagePicker with SwiftUI.
struct CaptureImageView: UIViewControllerRepresentable {
    @Binding private var isShown: Bool
    @Binding private var sourceType: UIImagePickerController.SourceType
    @Binding private var uiImage: UIImage?
    
    init(isShown: Binding<Bool>, sourceType: Binding<UIImagePickerController.SourceType>, uiImage: Binding<UIImage?>) {
        _isShown = isShown
        _sourceType = sourceType
        _uiImage = uiImage
    }
    
    func makeCoordinator() -> CaptureImageView.Coordinator {
        return Coordinator(isShown: $isShown, sourceType: $sourceType, uiImage: $uiImage)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<CaptureImageView>) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        picker.allowsEditing = true
        picker.sourceType = sourceType
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: UIViewControllerRepresentableContext<CaptureImageView>) {
    }
    
    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        @Binding var isCoordinatorShown: Bool
        @Binding var imageSourceType: UIImagePickerController.SourceType
        @Binding var uiImage: UIImage?
        
        init(isShown: Binding<Bool>, sourceType: Binding<UIImagePickerController.SourceType>, uiImage: Binding<UIImage?>) {
            _isCoordinatorShown = isShown
            _imageSourceType = sourceType
            _uiImage = uiImage
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
            let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            
            if let unwrapImage = editedImage {
                uiImage = unwrapImage
            } else if let unwrapImage = originalImage {
                uiImage = unwrapImage
            }
            isCoordinatorShown = false
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            isCoordinatorShown = false
        }
    }
}
