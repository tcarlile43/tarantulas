//
//  TarantulaListCellView.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/27/20.
//

import SwiftUI

struct TarantulaListCellView: View {
    @ObservedObject private var tarantula: Tarantula
    
    init(tarantula: Tarantula) {
        self.tarantula = tarantula
    }
    
    var body: some View {
        HStack {
            (tarantula.modelImage ?? Image.CAMERA)
                .resizable()
                .frame(width: Image.WIDTH, height: Image.HEIGHT, alignment: .topLeading)
            VStack(alignment: .leading) {
                Text(tarantula.modelName)
                    .font(.headline)
                Divider()
                Text(tarantula.modelGenus)
                    .font(.headline)
                Divider()
                Text(tarantula.modelSpecies)
                    .font(.headline)
                if !tarantula.modelCommonName.isEmpty {
                    Divider()
                    Text(tarantula.modelCommonName)
                        .font(.headline)
                }
            }
            .frame(minWidth: 0, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, maxHeight: .infinity)
        }
        .frame(minWidth: 0, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, minHeight: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, maxHeight: .infinity)
    }
}
