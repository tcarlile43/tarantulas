//
//  ImageSelector.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 1/24/21.
//

import Foundation
import SwiftUI

/// Modifier to create an image selector. This is usually done at the tap of the image that is to be changed.
/// This modifier allows the user to select a new photo from the photo library, take a new photo, cancel the
/// selection, or clear the current selection.
struct ImageSelector: ViewModifier {
    @State private var showCaptureImageView = false
    @State private var imageSourceType: UIImagePickerController.SourceType = .photoLibrary
    @State private var showActionSheet: Bool = false
    @Binding private var uiImage: UIImage?
    private let destroy: () -> Void
    
    init(uiImage: Binding<UIImage?>, destroy: @escaping () -> Void) {
        _uiImage = uiImage
        self.destroy = destroy
    }
    
    func body(content: Content) -> some View {
        content
            .onTapGesture {
                showActionSheet.toggle()
            }
            .actionSheet(isPresented: $showActionSheet) {
                var buttons: [ActionSheet.Button] = [.cancel(), .destructive(Text("Clear")) {
                    destroy()
                }]
                
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                    buttons.insert(.default(Text("Select Photo")) {
                        imageSourceType = .photoLibrary
                        showCaptureImageView.toggle()
                    }, at: 0)
                }
                
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    buttons.insert(.default(Text("Take Photo")) {
                        imageSourceType = .camera
                        showCaptureImageView.toggle()
                    }, at: 0)
                }
                
                return ActionSheet(title: Text("Select Photo"), message: Text("How would you like to select your photo?"), buttons: buttons)
            }
            .sheet(isPresented: $showCaptureImageView) {
                CaptureImageView(isShown: $showCaptureImageView, sourceType: $imageSourceType, uiImage: $uiImage)
            }
    }
}

extension Image {
    public static let DEFAULT: Image = .CAMERA
}
