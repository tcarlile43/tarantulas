//
//  FeedingView.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 12/2/20.
//

import SwiftUI

struct NewFeedingView: View {
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.managedObjectContext) private var viewContext
    
    @StateObject private var feeding: Feeding = Feeding.newModel()
    @ObservedObject private var tarantula: Tarantula
    @ObservedObject private var offerings = Offerings()
    @State private var userMessage: String = ""
    @State private var displayMessage: Bool = false
    @State private var presentOfferingSheet: Bool = false
    
    init(tarantula: Tarantula) {
        self.tarantula = tarantula
    }
    
    var body: some View {
        Form {
            DatePicker("Date Fed", selection: $feeding.modelDate, displayedComponents: .date)
            Section(header: Text("Notes")) {
                TextView(text: $feeding.modelNotes).frame(minHeight: 200)
            }
            List {
                Section(header: Text("Food Offered")) {
                    Button(action: {
                        presentOfferingSheet.toggle()
                    }) {
                        Text("New Offering").foregroundColor(.blue)
                    }
                    ForEach(offerings.offerings, id: \.self) { offering in
                        Text("\(offering.prey): \(offering.numEaten)/\(offering.numOffered)")
                    }
                    .onDelete(perform: {
                        guard let index = $0.first else {
                            return
                        }
                        
                        offerings.offerings.remove(at: index)
                    })
                }
            }
            Button(action: {
                UIApplication.endEditing()
                
                let managedFeeding = feeding.copyTo(context: viewContext)
                managedFeeding.tarantula = tarantula
                
                for offering in offerings.offerings {
                    let managedOffering = offering.copyTo(context: viewContext)
                    managedOffering.feeding = managedFeeding
                }
                
                feeding.resetModel()
                
                do {
                    try viewContext.saveSafely()
                    presentationMode.wrappedValue.dismiss()
                } catch {
                    userMessage = "\(error): \(error.localizedDescription)"
                    displayMessage.toggle()
                }
            }) {
                Text("Save").foregroundColor(.blue)
            }
        }
        .keyboardStyle()
        .alert(isPresented: $displayMessage) {
            Alert(title: Text("Save Failure"), message: Text(userMessage), dismissButton: .default(Text("Ok")))
        }.sheet(isPresented: $presentOfferingSheet) {
            NewOfferingView(feeding: feeding, offerings: offerings)
        }
    }
}

class Offerings: ObservableObject {
    @Published var offerings = [Offering]()
}

struct FeedingDetailView: View {
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.managedObjectContext) private var viewContext
    
    @ObservedObject private var feeding: Feeding
    @State private var userMessage: String = ""
    @State private var displayMessage: Bool = false
    
    init(feeding: Feeding) {
        self.feeding = feeding
    }
    
    var body: some View {
        Form {
            DatePicker("Date Fed", selection: $feeding.modelDate, displayedComponents: .date)
            Section(header: Text("Notes")) {
                TextView(text: $feeding.modelNotes).frame(minHeight: 200)
            }
            List {
                Section(header: Text("Food Offered")) {
                    ForEach(feeding.modelOfferings, id: \.self) { offering in
                        Text("\(offering.prey): \(offering.numEaten)/\(offering.numOffered)")
                    }
                }
            }
            Button(action: {
                do {
                    try viewContext.saveSafely()
                    presentationMode.wrappedValue.dismiss()
                } catch {
                    userMessage = "\(error): \(error.localizedDescription)"
                    displayMessage.toggle()
                }
            }) {
                Text("Update").foregroundColor(.blue)
            }
        }
        .alert(isPresented: $displayMessage) {
            Alert(title: Text("Save Failure"), message: Text(userMessage), dismissButton: .default(Text("Ok")))
        }
    }
}

/// TODO - unused
struct HungryView: View {
    private static let DATE_FORMATTER: DateFormatter = {
            let d = DateFormatter()
            d.dateFormat = "MMM d, y"
            return d
        }()
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @FetchRequest(entity: Tarantula.entity(),
                  sortDescriptors: [NSSortDescriptor(keyPath: \Tarantula.nextFeeding, ascending: true)],
                  predicate: NSPredicate(format: "feedingFrequency > 0 AND nextFeeding != nil"),
                  animation: .default)
    private var tarantulas: FetchedResults<Tarantula>
    
    var body: some View {
        List {
            Section(header: Text("Hungry Tarantulas")) {
                ForEach(tarantulas, id: \.self) { tarantula in
                    Text("Feed \(tarantula.modelName) on \(HungryView.DATE_FORMATTER.string(from: tarantula.nextFeeding ?? Date()))")
                }
            }
        }
        .navigationTitle("Hungry Tarantulas")
    }
}
