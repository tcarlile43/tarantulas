//
//  TextView.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/24/20.
//

import SwiftUI

/// Wrap a UITextView into a View that can be used by SwiftUI
struct TextView: UIViewRepresentable {
    private static let DEFAULT_FONT: CGFloat = 17
    
    typealias UIViewType = UITextView
    
    @Binding private var text: String
    private let fontSize: CGFloat
    
    init(text: Binding<String>, fontSize: CGFloat = TextView.DEFAULT_FONT) {
        self._text = text
        self.fontSize = fontSize
    }
    
    func makeUIView(context: UIViewRepresentableContext<TextView>) -> UITextView {
        let textView = UITextView()
        textView.isScrollEnabled = true
        textView.isEditable = true
        textView.isUserInteractionEnabled = true
        textView.font = UIFont.systemFont(ofSize: fontSize)
        return textView
    }
    
    func updateUIView(_ uiView: UITextView, context: UIViewRepresentableContext<TextView>) {
        uiView.text = text
        uiView.delegate = context.coordinator
    }
    
    func makeCoordinator() -> TextView.Coordinator {
        return Coordinator(self)
    }
    
    class Coordinator: NSObject, UITextViewDelegate {
        var parent: TextView
        
        init(_ parent: TextView) {
            self.parent = parent
        }
        
        func textViewDidChange(_ textView: UITextView) {
            parent.text = textView.text
        }
    }
}
