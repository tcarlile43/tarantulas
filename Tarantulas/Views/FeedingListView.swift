//
//  FeedingListView.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 12/2/20.
//

import SwiftUI

struct FeedingListView: View {
    private static let DATE_FORMATTER: DateFormatter = {
            let d = DateFormatter()
            d.dateFormat = "MMM d, y"
            return d
        }()
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @ObservedObject private var tarantula: Tarantula
    @FetchRequest private var feedings: FetchedResults<Feeding>
    @State private var userMessage: String = ""
    @State private var displayMessage: Bool = false
    
    init(tarantula: Tarantula) {
        self.tarantula = tarantula
        self._feedings = FetchRequest(entity: Feeding.entity(),
                                      sortDescriptors: [NSSortDescriptor(keyPath: \Feeding.date, ascending: false)],
                                      predicate: NSPredicate(format: "tarantula = %@", tarantula),
                                      animation: .default)
    }
    
    var body: some View {
        List {
            Section(header: Text("Feeding Frequency")) {
                Stepper(value: $tarantula.modelFeedingFrequency, in: 0...365, onEditingChanged: {
                    if !$0 {
                        do {
                            try viewContext.saveSafely()
                        } catch {
                            userMessage = "\(error): \(error.localizedDescription)"
                            displayMessage.toggle()
                        }
                    }
                }) {
                    Text(tarantula.modelFeedingFrequency == 0 ? "Unspecified" : tarantula.modelFeedingFrequency == 1 ? "Every Day" : "Every \(tarantula.modelFeedingFrequency) Days")
                }
            }
            Section(header: Text("Feedings")) {
                NavigationLink(destination: NewFeedingView(tarantula: tarantula)) {
                    Text("Feed").foregroundColor(.blue)
                }
                ForEach(feedings, id: \.self) { feeding in
                    NavigationLink(destination: FeedingDetailView(feeding: feeding)) {
                        Text(FeedingListView.DATE_FORMATTER.string(from: feeding.date))
                    }
                }
                .onDelete(perform: deleteItems)
            }
        }
        .alert(isPresented: $displayMessage) {
            Alert(title: Text("Save Failure"), message: Text(userMessage), dismissButton: .default(Text("Ok")))
        }
    }
    
    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            // TODO: This is necessary due to a system bug. See: https://developer.apple.com/forums/thread/668299
            viewContext.perform {
                offsets.map { feedings[$0] }.forEach(viewContext.delete)

                do {
                    try viewContext.saveSafely()
                } catch {
                    userMessage = "\(error): \(error.localizedDescription)"
                    displayMessage.toggle()
                }
            }
        }
    }
}
