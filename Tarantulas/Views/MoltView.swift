//
//  MoltView.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/29/20.
//

import SwiftUI

struct MoltView: View {
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.managedObjectContext) private var viewContext
    
    @ObservedObject private var tarantula: Tarantula
    @ObservedObject private var molt: Molt
    @State private var image: Image? = .CAMERA
    @State private var shouldIncreaseInstar: Bool = false
    @State private var userMessage: String = ""
    @State private var displayMessage: Bool = false
    @State private var unsexed: Bool
    @State private var isTarantulaMature: Bool
    
    init(tarantula: Tarantula, molt: Molt) {
        self.tarantula = tarantula
        self.molt = molt
        self._unsexed = State(wrappedValue: tarantula.modelGender == .Unsexed)
        self._isTarantulaMature = State(wrappedValue: tarantula.isMature)
    }
    
    var currentImage: Image {
        molt.modelImage ?? Image.CAMERA
    }
    
    var body: some View {
        Form {
            Section(header: Text("Picture")) {
                GeometryReader { _ in
                    currentImage
                        .resizable()
                }
                .aspectRatio(1.0, contentMode: .fit)
                .modifier(ImageSelector(uiImage: $molt.modelPhoto) {
                    molt.photo = nil
                    image = .DEFAULT
                })
            }
            Section(header: Text("Details")) {
                DatePicker("Date of Molt", selection: $molt.modelDate, displayedComponents: .date)
                if !molt.isManaged() && unsexed {
                    Picker("Gender", selection: $tarantula.modelGender) {
                        ForEach(Gender.allCases, id: \.self) {
                            Text($0.rawValue).tag($0)
                        }
                    }
                }
                if !molt.isManaged() && !isTarantulaMature {
                    Toggle("Is Mature", isOn: $tarantula.isMature)
                    if tarantula.isMature {
                        DatePicker("Date Matured", selection: $tarantula.modelDateMatured, displayedComponents: .date)
                    }
                }
            }
            if !molt.isManaged() {
                Section(header: Text("Size")) {
                    Stepper(value: $tarantula.modelSize, step: .SIZE_STEP) {
                        Text("Size: \(tarantula.modelSize, specifier: "%.2f")")
                    }
                    Picker("Unit", selection: $tarantula.modelSizeUnit) {
                        ForEach(SizeUnit.allCases, id: \.self) {
                            Text($0.rawValue).tag($0)
                        }
                    }
                    Toggle("Increment Instar", isOn: $shouldIncreaseInstar)
                }
            }
            Section(header: Text("Notes")) {
                TextView(text: $tarantula.modelNotes).frame(minHeight: 200)
            }
            Section() {
                Button(action: {
                    UIApplication.endEditing()
                    
                    var managedMolt = molt
                    if !molt.isManaged() {
                        managedMolt = molt.copyTo(context: viewContext)
                        molt.resetModel()
                    }
                    
                    managedMolt.tarantula = tarantula
                    
                    if shouldIncreaseInstar {
                        let _ = ++tarantula.modelInstar
                    }
                    
                    do {
                        try viewContext.saveSafely()
                        presentationMode.wrappedValue.dismiss()
                    } catch  {
                        userMessage = "\(error): \(error.localizedDescription)"
                        displayMessage.toggle()
                    }
                }) {
                    Text("Save Molt")
                }
            }
        }
        .navigationTitle(Text(molt.isManaged() ? "Molt" : "New Nolt"))
        .keyboardStyle()
        .alert(isPresented: $displayMessage) {
            Alert(title: Text("Save Failure"), message: Text(userMessage), dismissButton: .default(Text("Ok")))
        }
        .onAppear {
            shouldIncreaseInstar = false
        }
    }
}
