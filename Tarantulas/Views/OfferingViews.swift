//
//  OfferingViews.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 12/2/20.
//

import SwiftUI

struct NewOfferingView: View {
    @Environment(\.presentationMode) var presentationMode
    
    @StateObject private var offering: Offering = Offering.newModel()
    @ObservedObject private var feeding: Feeding
    @ObservedObject private var offerings: Offerings
    
    init(feeding: Feeding, offerings: Offerings) {
        self.feeding = feeding
        self.offerings = offerings
    }
    
    var body: some View {
        Form {
            TextField("Prey", text: $offering.modelPrey)
            Stepper(value: $offering.modelNumOffered, in: 1...500, onEditingChanged: {
                if !$0 {
                    offering.modelNumEaten = min(offering.modelNumEaten, offering.modelNumOffered)
                }
            }) {
                Text("Number Offered: \(offering.modelNumOffered)")
            }
            Stepper(value: $offering.modelNumEaten, in: 0...offering.modelNumOffered) {
                Text("Number Eaten: \(offering.modelNumEaten)")
            }
            Button(action: {
                UIApplication.endEditing()
                offerings.offerings.append(offering.copyModel())
                offering.resetModel()
                presentationMode.wrappedValue.dismiss()
            }) {
                Text("Save Offering").foregroundColor(.blue)
            }
        }
        .keyboardStyle()
    }
}
