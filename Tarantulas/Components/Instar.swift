//
//  Instar.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/24/20.
//

import Foundation

enum Instar: CustomStringConvertible {
    case Unknown, EWL, Instar(Int)
    
    init(rawValue: Int) {
        if rawValue < 0 {
            self = .Unknown
        } else if rawValue == 0 {
            self = .EWL
        } else {
            self = .Instar(rawValue)
        }
    }
    
    var description: String {
        switch self {
        case .Unknown:
            return "Unknown"
        case .EWL:
            return "Eggs with Legs"
        case .Instar(let instar):
            return "\(instar)"
        }
    }
    
    var rawValue: Int {
        switch self {
        case .Unknown:
            return -1
        case .EWL:
            return 0
        case .Instar(let instar):
            return instar
        }
    }
}

extension Instar: Strideable {
    func advanced(by n: Int) -> Instar {
        return self + n
    }
    
    func distance(to other: Instar) -> Int {
        return (other - self).rawValue
    }
}

extension Instar: Equatable {
    static func ==(left: Instar, right: Instar) -> Bool {
        return left.rawValue == right.rawValue
    }
}

extension Instar {
    static func +=(left: inout Instar, right: Int) -> Instar {
        left = Tarantulas.Instar(rawValue: left.rawValue + right)
        return left
    }
    
    static func -=(left: inout Instar, right: Int) -> Instar {
        left = Tarantulas.Instar(rawValue: left.rawValue - right)
        return left
    }
    
    static func +(left: Instar, right: Int) -> Instar {
        return Tarantulas.Instar(rawValue: left.rawValue + right)
    }
    
    static func +(left: Int, right: Instar) -> Instar {
        return Tarantulas.Instar(rawValue: left + right.rawValue)
    }
    
    static func -(left: Instar, right: Instar) -> Instar {
        return Tarantulas.Instar(rawValue: left.rawValue - right.rawValue)
    }
    
    static func -(left: Instar, right: Int) -> Instar {
        return Tarantulas.Instar(rawValue: left.rawValue - right)
    }
    
    static func -(left: Int, right: Instar) -> Instar {
        return Tarantulas.Instar(rawValue: left - right.rawValue)
    }
    
    static prefix func ++(left: inout Instar) -> Instar {
        left = Tarantulas.Instar(rawValue: 1 + left.rawValue)
        return left
    }
    
    static postfix func ++(left: inout Instar) -> Instar {
        let current = left
        left = Tarantulas.Instar(rawValue: 1 + left.rawValue)
        return current
    }
    
    static prefix func --(left: inout Instar) -> Instar {
        left = Tarantulas.Instar(rawValue: left.rawValue - 1)
        return left
    }
    
    static postfix func --(left: inout Instar) -> Instar {
        let current = left
        left = Tarantulas.Instar(rawValue: left.rawValue - 1)
        return current
    }
}

extension Instar {
    public static let DEFAULT: Instar = .Unknown
}
