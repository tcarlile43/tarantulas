//
//  Behavior.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/24/20.
//

import Foundation

enum Behavior: String, CaseIterable {
    case Fossorial, Terrestrial, Semi_Arboreal = "Semi Arboreal", Arboreal
}

extension Behavior {
    public static let DEFAULT: Behavior = .Terrestrial
}
