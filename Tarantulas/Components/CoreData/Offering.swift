//
//  Offering.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 12/2/20.
//

import Foundation
import CoreData

class Offering: NSManagedObject, Identifiable {
    @NSManaged public var prey: String
    @NSManaged public var numOffered: Int16
    @NSManaged public var numEaten: Int16
    @NSManaged public var feeding: Feeding
    
    override public func willChangeValue(forKey key: String) {
        super.willChangeValue(forKey: key)
        self.objectWillChange.send()
    }
    
    func copyTo(context: NSManagedObjectContext) -> Offering {
        let offering = Offering(context: context)
        offering.prey = prey
        offering.numOffered = numOffered
        offering.numEaten = numEaten
        return offering
    }
    
    func isManaged() -> Bool {
        return managedObjectContext != nil
    }
}

extension Offering {
    static func newModel() -> Offering {
        let offering = Offering(entity: Offering.entity(), insertInto: nil)
        offering.resetModel()
        return offering
    }
    
    func copyModel() -> Offering {
        let offering = Offering(entity: Offering.entity(), insertInto: nil)
        offering.prey = prey
        offering.numOffered = numOffered
        offering.numEaten = numEaten
        return offering
    }
    
    var modelPrey: String {
        get {
            prey
        }
        set {
            prey = newValue
        }
    }
    
    var modelNumOffered: Int {
        get {
            Int(numOffered)
        }
        set {
            numOffered = Int16(newValue)
        }
    }
    
    var modelNumEaten: Int {
        get {
            Int(numEaten)
        }
        set {
            numEaten = Int16(newValue)
        }
    }
    
    func resetModel() {
        prey = ""
        numOffered = 1
        numEaten = 0
    }
}
