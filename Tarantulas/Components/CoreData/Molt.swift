//
//  Molt.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/29/20.
//

import Foundation
import CoreData
import SwiftUI

public class Molt: NSManagedObject, Identifiable {
    @NSManaged public var date: Date
    @NSManaged public var notes: String?
    @NSManaged public var tarantula: Tarantula
    @NSManaged public var photo: Data?
    
    override public func willChangeValue(forKey key: String) {
        super.willChangeValue(forKey: key)
        self.objectWillChange.send()
    }
    
    func isManaged() -> Bool {
        return managedObjectContext != nil
    }
    
    func copyTo(context: NSManagedObjectContext) -> Molt {
        let molt = Molt(context: context)
        molt.date = date
        molt.notes = notes
        molt.photo = photo
        return molt
    }
}

extension Molt {
    static func newModel() -> Molt {
        let molt = Molt(entity: Molt.entity(), insertInto: nil)
        molt.resetModel()
        return molt
    }
    
    var modelDate: Date {
        get {
            date
        }
        set {
            date = newValue
        }
    }
    
    var modelNotes: String {
        get {
            notes ?? ""
        }
        set {
            notes = newValue
        }
    }
    
    var modelImage: Image? {
        if let image = modelPhoto {
            return Image(uiImage: image)
        } else {
            return nil
        }
    }
    
    var modelPhoto: UIImage? {
        get {
            if let photo = photo {
                return UIImage(data: photo)
            } else {
                return nil
            }
        }
        set {
            photo = newValue?.pngData()
        }
    }
    
    func resetModel() {
        date = Date()
        notes = nil
        photo = nil
    }
}
