//
//  Feeding.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 12/2/20.
//

import Foundation
import CoreData

class Feeding: NSManagedObject, Identifiable {
    @NSManaged public var date: Date
    @NSManaged public var notes: String?
    @NSManaged public var tarantula: Tarantula
    @NSManaged public var offerings: NSSet?
    
    override public func willChangeValue(forKey key: String) {
        super.willChangeValue(forKey: key)
        self.objectWillChange.send()
    }
    
    func copyTo(context: NSManagedObjectContext) -> Feeding {
        let feeding = Feeding(context: context)
        feeding.date = date
        feeding.notes = notes
        return feeding
    }
    
    func isManaged() -> Bool {
        return managedObjectContext != nil
    }
}

extension Feeding {
    static func newModel() -> Feeding {
        let feeding = Feeding(entity: Feeding.entity(), insertInto: nil)
        feeding.resetModel()
        return feeding
    }
    
    var modelDate: Date {
        get {
            date
        }
        set {
            date = newValue
        }
    }
    
    var modelNotes: String {
        get {
            notes ?? ""
        }
        set {
            notes = newValue
        }
    }
    
    var modelOfferings: [Offering] {
        get {
            guard let array = offerings?.allObjects as? [Offering] else {
                return []
            }
            
            return array
        }
    }
    
    func resetModel() {
        date = Date()
        notes = nil
    }
}
