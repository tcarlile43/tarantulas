//
//  Tarantula.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/24/20.
//

import Foundation
import CoreData
import UIKit
import SwiftUI

public class Tarantula: NSManagedObject, Identifiable {
    @NSManaged public var photo: Data?
    @NSManaged public var name: String?
    @NSManaged public var genus: String?
    @NSManaged public var species: String?
    @NSManaged public var gender: String
    @NSManaged public var world: String
    @NSManaged public var behavior: String
    @NSManaged public var size: Double
    @NSManaged public var sizeUnit: String
    @NSManaged public var instar: Int16
    @NSManaged public var dateAcquired: Date?
    @NSManaged public var dateDeceased: Date?
    @NSManaged public var dateMatured: Date?
    @NSManaged public var notes: String?
    @NSManaged public var molts: NSSet?
    @NSManaged public var feedingFrequency: Int16
    @NSManaged public var feedings: NSSet?
    @NSManaged public var nextFeeding: Date?
    @NSManaged public var isMature: Bool
    @NSManaged public var isDeceased: Bool
    @NSManaged public var dob: Date?
    @NSManaged public var commonName: String?
    
    override public func willChangeValue(forKey key: String) {
        super.willChangeValue(forKey: key)
        self.objectWillChange.send()
    }
    
    func copyTo(context: NSManagedObjectContext) -> Tarantula {
        let tarantula = Tarantula(context: context)
        tarantula.photo = photo
        tarantula.name = name
        tarantula.genus = genus
        tarantula.species = species
        tarantula.gender = gender
        tarantula.world = world
        tarantula.behavior = behavior
        tarantula.size = size
        tarantula.sizeUnit = sizeUnit
        tarantula.instar = instar
        tarantula.dateAcquired = dateAcquired
        tarantula.dateDeceased = dateDeceased
        tarantula.dateMatured = dateMatured
        tarantula.notes = notes
        tarantula.feedingFrequency = feedingFrequency
        tarantula.isMature = isMature
        tarantula.isDeceased = isDeceased
        tarantula.commonName = commonName
        tarantula.dob = dob
        return tarantula
    }
    
    func isManaged() -> Bool {
        return managedObjectContext != nil
    }
    
    override public func willSave() {
        if feedingFrequency > 0 {
            guard let feedings = feedings?.allObjects as? [Feeding] else {
                return
            }
            
            let dates = feedings.map { $0.date }.sorted { $0.compare($1) == .orderedDescending }
            
            guard let mostRecentFeeding = dates.first else {
                return
            }
            
            let next = Calendar.current.date(byAdding: .day, value: Int(feedingFrequency), to: mostRecentFeeding)!
            let components = Calendar.current.dateComponents([.year, .month, .day], from: next)
            let nextFeeding = Calendar.current.date(from: components)
            
            if nextFeeding != self.nextFeeding {
                self.nextFeeding = nextFeeding
            }
        }
    }
}

extension Tarantula {
    static func newModel() -> Tarantula {
        let tarantula = Tarantula(entity: Tarantula.entity(), insertInto: nil)
        tarantula.resetModel()
        return tarantula
    }
    
    var modelImage: Image? {
        if let image = modelPhoto {
            return Image(uiImage: image)
        } else {
            return nil
        }
    }
    
    var modelPhoto: UIImage? {
        get {
            if let photo = photo {
                return UIImage(data: photo)
            } else {
                return nil
            }
        }
        set {
            photo = newValue?.pngData()
        }
    }
    
    var modelName: String {
        get {
            name ?? ""
        }
        set {
            name = newValue
        }
    }
    
    var modelGenus: String {
        get {
            genus ?? ""
        }
        set {
            genus = newValue
        }
    }
    
    var modelSpecies: String {
        get {
            species ?? ""
        }
        set {
            species = newValue
        }
    }
    
    var modelGender: Gender {
        get {
            Gender(rawValue: gender) ?? Gender.DEFAULT
        }
        set {
            gender = newValue.rawValue
        }
    }
    
    var modelWorld: World {
        get {
            World(rawValue: world) ?? World.DEFAULT
        }
        set {
            world = newValue.rawValue
        }
    }
    
    var modelBehavior: Behavior {
        get {
            Behavior(rawValue: behavior) ?? Behavior.DEFAULT
        }
        set {
            behavior = newValue.rawValue
        }
    }
    
    var modelSize: Double {
        get {
            size
        }
        set {
            size = newValue
        }
    }
    
    var modelSizeUnit: SizeUnit {
        get {
            SizeUnit(rawValue: sizeUnit) ?? SizeUnit.DEFAULT
        }
        set {
            sizeUnit = newValue.rawValue
        }
    }
    
    var modelInstar: Instar {
        get {
            Instar(rawValue: Int(instar))
        }
        set {
            instar = Int16(newValue.rawValue)
        }
    }
    
    var modelDateAcquired: Date {
        get {
            dateAcquired ?? Date()
        }
        set {
            dateAcquired = newValue
        }
    }
    
    var modelDateDeceased: Date {
        get {
            dateDeceased ?? Date()
        }
        set {
            dateDeceased = newValue
        }
    }
    
    var modelDateMatured: Date {
        get {
            dateMatured ?? Date()
        }
        set {
            dateMatured = newValue
        }
    }
    
    var modelNotes: String {
        get {
            notes ?? ""
        }
        set {
            notes = newValue
        }
    }
    
    var modelFeedingFrequency: Int {
        get {
            Int(feedingFrequency)
        }
        set {
            feedingFrequency = Int16(newValue)
        }
    }
    
    var modelDob: Date {
        get {
            dob ?? Date()
        }
        set {
            dob = Date()
        }
    }
    
    var modelCommonName: String {
        get {
            commonName ?? ""
        }
        set {
            commonName = newValue
        }
    }
    
    func resetModel() {
        photo = nil
        name = nil
        genus = nil
        species = nil
        gender = Gender.DEFAULT.rawValue
        world = World.DEFAULT.rawValue
        behavior = Behavior.DEFAULT.rawValue
        size = 0.0
        sizeUnit = SizeUnit.DEFAULT.rawValue
        instar = Int16(Instar.DEFAULT.rawValue)
        dateAcquired = Date()
        dateDeceased = nil
        dateMatured = nil
        notes = nil
        feedingFrequency = 0
        isMature = false
        isDeceased = false
        dob = nil
        commonName = nil
    }
}

extension Tarantula {
    var tarantulaGender: Gender {
        get {
            Gender(rawValue: gender) ?? Gender.DEFAULT
        }
        set {
            gender = newValue.rawValue
        }
    }
    
    var uiImage: UIImage? {
        guard let photo = photo else {
            return nil
        }
        
        if let uiImage = UIImage(data: photo) {
            return uiImage
        } else {
            return nil
        }
    }
    
    var image: Image? {
        if let uiImage = self.uiImage {
            return Image(uiImage: uiImage)
        } else {
            return nil
        }
    }
}

internal enum SortKey: Int, Hashable, CaseIterable, CustomStringConvertible {
    case Name
    case Genus
    case Species
    case Size
    case Instar
    case Date_Acquired
    case Gender
    case World
    case Behavior
    
    var description: String {
        switch self {
        case .Name:
            return "Name"
        case .Genus:
            return "Genus"
        case .Species:
            return "Species"
        case .Size:
            return "Size"
        case .Instar:
            return "Instar"
        case .Date_Acquired:
            return "Date Acquired"
        case .Gender:
            return "Gender"
        case .World:
            return "World"
        case .Behavior:
            return "Behavior"
        }
    }
    
    func toSortDescriptor(ascending: Bool) -> NSSortDescriptor {
        switch self {
        case .Name:
            return NSSortDescriptor(keyPath: \Tarantula.name, ascending: ascending)
        case .Genus:
            return NSSortDescriptor(keyPath: \Tarantula.genus, ascending: ascending)
        case .Species:
            return NSSortDescriptor(keyPath: \Tarantula.species, ascending: ascending)
        case .Size:
            return NSSortDescriptor(keyPath: \Tarantula.size, ascending: ascending)
        case .Instar:
            return NSSortDescriptor(keyPath: \Tarantula.instar, ascending: ascending)
        case .Date_Acquired:
            return NSSortDescriptor(keyPath: \Tarantula.dateAcquired, ascending: ascending)
        case .Gender:
            return NSSortDescriptor(keyPath: \Tarantula.gender, ascending: ascending)
        case .World:
            return NSSortDescriptor(keyPath: \Tarantula.world, ascending: ascending)
        case .Behavior:
            return NSSortDescriptor(keyPath: \Tarantula.behavior, ascending: ascending)
        }
    }
}
