//
//  World.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/24/20.
//

import Foundation

enum World: String, CaseIterable {
    case Old = "Old World", New = "New World"
}

extension World {
    public static let DEFAULT: World = .New
}
