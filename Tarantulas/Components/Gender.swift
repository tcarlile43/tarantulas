//
//  Gender.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/24/20.
//

import Foundation

enum Gender: String, CaseIterable {
    case Male, Female, Unsexed
}

extension Gender {
    public static let DEFAULT: Gender = .Unsexed
    
    public static func toShorthandNotation<T: Sequence>(_ genders: T) -> String where T.Element == Gender {
        var counts: [Gender: Int] = [:]
        
        for gender in genders {
            counts[gender, default: 0] += 1
        }
        
        return "\(counts[.Male, default: 0]).\(counts[.Female, default: 0]).\(counts[.Unsexed, default: 0])"
    }
}
