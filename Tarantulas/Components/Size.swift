//
//  Size.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/24/20.
//

import Foundation

enum SizeUnit: String, CaseIterable {
    case IN, CM
}

extension SizeUnit {
    public static let DEFAULT: SizeUnit = .CM
    
    public static func convert(from: SizeUnit, to: SizeUnit, size: Double) -> Double {
        return converterFor(from: from, to: to)(size)
    }
    
    private static func converterFor(from: SizeUnit, to: SizeUnit) -> (Double) -> Double {
        if from == to {
            return { $0 }
        } else if from == .CM {
            return { $0.inches }
        } else {
            return { $0.centimeters }
        }
    }
}

extension Double {
    public static let SIZE_STEP = 0.25
}

fileprivate let IN_CM_CONVERSION = 2.54

typealias Inches = Double
extension Inches {
    var centimeters: Centimeters {
        self * IN_CM_CONVERSION
    }
}

typealias Centimeters = Double
extension Centimeters {
    var inches: Inches {
        self / IN_CM_CONVERSION
    }
}
