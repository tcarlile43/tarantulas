//
//  Loan.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 1/10/21.
//

import Foundation

enum Loan: CustomStringConvertible {
    case Outright, Partial(Int)
    
    init(rawValue: Int) {
        if rawValue >= 100 {
            self = .Outright
        } else {
            self = .Partial(max(0, rawValue))
        }
    }
    
    var description: String {
        switch self {
        case .Outright:
            return "Outright"
        case .Partial(let percentage):
            return "\(percentage)"
        }
    }
    
    var rawValue: Int {
        switch self {
        case .Outright:
            return 100
        case .Partial(let percentage):
            return percentage
        }
    }
}
