//
//  Commons.swift
//  Tarantulas
//
//  Created by Tyler Carlile on 11/27/20.
//

import Foundation
import SwiftUI
import CoreData

extension Sequence where Iterator.Element: Hashable {
    func unique<T>(keyExtractor: (Element) -> T = { $0 as! T }) -> [Iterator.Element] where T: Hashable {
        var seen: Set<T> = []
        return filter { seen.insert(keyExtractor($0)).inserted }
    }
}

internal extension Image {
    static let CAMERA = Image(systemName: "camera")
    static let HEIGHT = CGFloat(128)
    static let WIDTH = CGFloat(128)
}

internal extension UIApplication {
    static func endEditing(uiApplication: UIApplication = .shared) {
        uiApplication.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

internal extension NSManagedObjectContext {
    func saveSafely() throws {
        do {
            try save()
        } catch {
            rollback()
            throw error
        }
    }
}

internal struct Settings {
    static let TARANTULA_SORT_KEY = "TarantulaSortKey"
    static let TARANTULA_SORT_ASCENDING = "TarantulaSortAscending"
    static let PREFERRED_SIZE_UNIT = "PreferredSizeUnit"
}

internal extension View {
    var screenWidth: CGFloat {
        UIScreen.main.bounds.width
    }
    var screenHeight: CGFloat {
        UIScreen.main.bounds.height
    }
}
